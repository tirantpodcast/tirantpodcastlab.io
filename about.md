---
layout: page
title: Contacte
permalink: /about/
---

+ email: <tirantpodcast@protonmail.com>
+ web: <https://tirantpodcast.wordpress.com/>
+ Feed Podcast: <https://tirantpodcast.gitlab.io/feed>
+ Twitter: <https://twitter.com/tirantpodcast>
+ Instagram: <https://www.instagram.com/tirantpodcast/>
+ Facebook: <https://www.facebook.com/tirant.loblanc.33>
